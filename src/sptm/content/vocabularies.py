#!/usr/bin/python
# -*- coding: utf-8 -*-

from zope.schema.interfaces import IVocabularyFactory
from zope.interface import implements
from zope.schema.vocabulary import SimpleVocabulary
from zope.schema.vocabulary import SimpleTerm
from sptm.content import _


class CgTypesVocabulary(object):
    """Vocabulary Factory for ChengGuo Types
    """
    implements(IVocabularyFactory)

    def __call__(self, *args, **kwargs):

        cg_types = {'Activity': u'活動成果', 'Research': u'研究成果', 'Project': u'主題成果'}
        cg_terms = [SimpleTerm(i[0], i[0], i[1]) for i in cg_types.items()]
        return SimpleVocabulary(cg_terms)

class ActCtgrVocabulary(object):
    """Vocabulary Factory for Activity Ctgr
    """
    implements(IVocabularyFactory)

    def __call__(self, *args, **kwargs):

        items = (
            SimpleTerm(value='a1', title=_(u'Camp')),
            SimpleTerm(value='a2', title=_(u'Seminar')),
            SimpleTerm(value='a3', title=_(u'Workshop')),
            SimpleTerm(value='a4', title=_(u'Conference')),
            SimpleTerm(value='a5', title=_(u'Expo')),
            SimpleTerm(value='a6', title=_(u'Participation')),
        )
        return SimpleVocabulary(items)

class RshCtgrVocabulary(object):
    """Vocabulary Factory for Research Ctgr
    """
    implements(IVocabularyFactory)

    def __call__(self, *args, **kwargs):

        items = (
            SimpleTerm(value='r1', title=_(u'Periodical Paper')),
            SimpleTerm(value='r2', title=_(u'Conference Paper')),
            SimpleTerm(value='r3', title=_(u'Book Paper')),
            SimpleTerm(value='r4', title=_(u'Book')),
            SimpleTerm(value='r5', title=_(u'Atlas'))
        )
        return SimpleVocabulary(items)

class PrjCtgrVocabulary(object):
    """Vocabulary Factory for Project Ctgr
    """
    implements(IVocabularyFactory)

    def __call__(self, *args, **kwargs):

        items = (
            SimpleTerm(value='p1', title=_(u'Illustration')),
            SimpleTerm(value='p2', title=_(u'Website')),
            SimpleTerm(value='p3', title=_(u'App')),
            SimpleTerm(value='p4', title=_(u'System')),
            SimpleTerm(value='p5', title=_(u'Other'))
        )
        return SimpleVocabulary(items)

