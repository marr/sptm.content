# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from plone import api
from sptm.content.testing import SPTM_CONTENT_INTEGRATION_TESTING  # noqa

import unittest


class TestSetup(unittest.TestCase):
    """Test that sptm.content is properly installed."""

    layer = SPTM_CONTENT_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if sptm.content is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'sptm.content'))

    def test_browserlayer(self):
        """Test that ISptmContentLayer is registered."""
        from sptm.content.interfaces import (
            ISptmContentLayer)
        from plone.browserlayer import utils
        self.assertIn(ISptmContentLayer, utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = SPTM_CONTENT_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        self.installer.uninstallProducts(['sptm.content'])

    def test_product_uninstalled(self):
        """Test if sptm.content is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'sptm.content'))

    def test_browserlayer_removed(self):
        """Test that ISptmContentLayer is removed."""
        from sptm.content.interfaces import \
            ISptmContentLayer
        from plone.browserlayer import utils
        self.assertNotIn(ISptmContentLayer, utils.registered_layers())
