# -*- coding: utf-8 -*-
from plone import api
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from plone.dexterity.interfaces import IDexterityFTI
from sptm.content.interfaces import IData
from sptm.content.testing import SPTM_CONTENT_INTEGRATION_TESTING  # noqa
from zope.component import createObject
from zope.component import queryUtility

import unittest


class DataIntegrationTest(unittest.TestCase):

    layer = SPTM_CONTENT_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_schema(self):
        fti = queryUtility(IDexterityFTI, name='Data')
        schema = fti.lookupSchema()
        self.assertEqual(IData, schema)

    def test_fti(self):
        fti = queryUtility(IDexterityFTI, name='Data')
        self.assertTrue(fti)

    def test_factory(self):
        fti = queryUtility(IDexterityFTI, name='Data')
        factory = fti.factory
        obj = createObject(factory)
        self.assertTrue(IData.providedBy(obj))

    def test_adding(self):
        obj = api.content.create(
            container=self.portal,
            type='Data',
            id='Data',
        )
        self.assertTrue(IData.providedBy(obj))
