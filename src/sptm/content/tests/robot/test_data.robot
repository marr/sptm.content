# ============================================================================
# DEXTERITY ROBOT TESTS
# ============================================================================
#
# Run this robot test stand-alone:
#
#  $ bin/test -s sptm.content -t test_data.robot --all
#
# Run this robot test with robot server (which is faster):
#
# 1) Start robot server:
#
# $ bin/robot-server --reload-path src sptm.content.testing.SPTM_CONTENT_ACCEPTANCE_TESTING
#
# 2) Run robot tests:
#
# $ bin/robot src/plonetraining/testing/tests/robot/test_data.robot
#
# See the http://docs.plone.org for further details (search for robot
# framework).
#
# ============================================================================

*** Settings *****************************************************************

Resource  plone/app/robotframework/selenium.robot
Resource  plone/app/robotframework/keywords.robot

Library  Remote  ${PLONE_URL}/RobotRemote

Test Setup  Open test browser
Test Teardown  Close all browsers


*** Test Cases ***************************************************************

Scenario: As a site administrator I can add a Data
  Given a logged-in site administrator
    and an add data form
   When I type 'My Data' into the title field
    and I submit the form
   Then a data with the title 'My Data' has been created

Scenario: As a site administrator I can view a Data
  Given a logged-in site administrator
    and a data 'My Data'
   When I go to the data view
   Then I can see the data title 'My Data'


*** Keywords *****************************************************************

# --- Given ------------------------------------------------------------------

a logged-in site administrator
  Enable autologin as  Site Administrator

an add data form
  Go To  ${PLONE_URL}/++add++Data

a data 'My Data'
  Create content  type=Data  id=my-data  title=My Data


# --- WHEN -------------------------------------------------------------------

I type '${title}' into the title field
  Input Text  name=form.widgets.title  ${title}

I submit the form
  Click Button  Save

I go to the data view
  Go To  ${PLONE_URL}/my-data
  Wait until page contains  Site Map


# --- THEN -------------------------------------------------------------------

a data with the title '${title}' has been created
  Wait until page contains  Site Map
  Page should contain  ${title}
  Page should contain  Item created

I can see the data title '${title}'
  Wait until page contains  Site Map
  Page should contain  ${title}
