# -*- coding: utf-8 -*-
"""Module where all interfaces, events and exceptions live."""

from sptm.content import _
from zope import schema
from zope.interface import Interface
from zope.publisher.interfaces.browser import IDefaultBrowserLayer
from plone.app.textfield import RichText
from plone.namedfile import field as namedfile
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm
from plone.supermodel import model


data_src = SimpleVocabulary(
    [SimpleTerm(value='gisc', title=_(u'GIS Center')),
     SimpleTerm(value='dnfl', title=_(u'Folklore')),
     SimpleTerm(value='road', title=_(u'GodRoad')),
     SimpleTerm(value='othr', title=_(u'Other'))]
)

device = SimpleVocabulary(
    [SimpleTerm(value='gps', title=_(u'GPS')),
     SimpleTerm(value='car', title=_(u'Car')),
     SimpleTerm(value='hnd', title=_(u'Handheld'))]
)

a_ctgr = SimpleVocabulary(
    [SimpleTerm(value='a1', title=_(u'Camp')),
     SimpleTerm(value='a2', title=_(u'Seminar')),
     SimpleTerm(value='a3', title=_(u'Workshop')),
     SimpleTerm(value='a4', title=_(u'Conference')),
     SimpleTerm(value='a5', title=_(u'Expo')),
     SimpleTerm(value='a6', title=_(u'Participation'))]
)

r_ctgr = SimpleVocabulary(
    [SimpleTerm(value='r1', title=_(u'Periodical Paper')),
     SimpleTerm(value='r2', title=_(u'Conference Paper')),
     SimpleTerm(value='r3', title=_(u'Book Paper')),
     SimpleTerm(value='r4', title=_(u'Book')),
     SimpleTerm(value='r5', title=_(u'Atlas'))]
)

p_ctgr = SimpleVocabulary(
    [SimpleTerm(value='p1', title=_(u'Illustration')),
     SimpleTerm(value='p2', title=_(u'Website')),
     SimpleTerm(value='p3', title=_(u'App')),
     SimpleTerm(value='p4', title=_(u'System')),
     SimpleTerm(value='p5', title=_(u'Other'))]
)


class ISptmContentLayer(IDefaultBrowserLayer):
    """Marker interface that defines a browser layer."""


class IData(Interface):

    d_start = schema.Date(
        title=_(u'Start'),
        required=False,
    )

    d_end = schema.Date(
        title=_(u'End'),
        required=False,
    )

    organizer = schema.TextLine(
        title=_(u'Organizer'),
        required=False,
    )
    model.fieldset(
        'ownership',
        label=_('label_schema_ownership', default=u'Ownership'),
        fields=['organizer'],
    )

    temple = RichText(
        title=_(u'Temple'),
        required=False,
    )

    data_src = schema.Choice(
        title=_(u'Data Source'),
        required=False,
        vocabulary=data_src,
    )

    device = schema.Choice(
        title=_(u'Device'),
        required=False,
        vocabulary=device,
    )

    d_fig = RichText(
        title=_(u'Figure'),
        required=False,
    )

    d_link = schema.TextLine(
        title=_(u'Link'),
        required=False,
    )

    d_text = RichText(
        title=_(u'Text'),
        required=False,
    )

    rite = RichText(
        title=_(u'Rite'),
        required=False,
    )


class IActivity(Interface):

    a_start = schema.Date(
        title=_(u'Start'),
        required=False,
    )

    a_end = schema.Date(
        title=_(u'End'),
        required=False,
    )

    a_text = RichText(
        title=_(u'Text'),
        required=False,
    )

    photo = RichText(
        title=_(u'Photo'),
        required=False,
    )

    a_ctgr = schema.Choice(
        title=_(u'Category'),
        required=False,
        vocabulary=a_ctgr,
    )


class IResearch(Interface):

    r_ctgr = schema.Choice(
        title=_(u'Category'),
        required=False,
        vocabulary=r_ctgr,
    )

    r_title = schema.TextLine(
        title=_(u'Publication'),
        required=False,
    )

    r_vol = schema.TextLine(
        title=_(u'Volumn'),
        required=False,
    )

    r_org = schema.TextLine(
        title=_(u'Publish Organization'),
        required=False,
    )

    r_date = schema.Date(
        title=_(u'Publish Date'),
        required=False,
    )

    r_text = RichText(
        title=_(u'Text'),
        required=False,
    )


class IProject(Interface):

    p_ctgr = schema.Choice(
        title=_(u'Category'),
        required=False,
        vocabulary=p_ctgr,
    )

    p_date = schema.Date(
        title=_(u'Project Date'),
        required=False,
    )

    p_text = RichText(
        title=_(u'Text'),
        required=False,
    )

